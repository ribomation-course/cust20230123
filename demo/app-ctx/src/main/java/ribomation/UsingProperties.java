package ribomation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import ribomation.domain.Car;
import ribomation.domain.Engine;
import ribomation.domain.Person;

public class UsingProperties {
    public static void main(String[] args) {
        var ctx = new AnnotationConfigApplicationContext(UsingProperties.Beans.class);
        System.out.println(ctx.getBean("person"));
    }

    @Configuration
    static class Beans {
        @Bean static PropertySourcesPlaceholderConfigurer props() {
            var p = new PropertySourcesPlaceholderConfigurer();
            p.setLocation(new ClassPathResource("/names.properties"));
            return p;
        }

        @Bean Person person(Car car,
                @Value("${person.name}") String name
        ) {
            var p = new Person();
            p.setName(name);
            p.setCar(car);
            return p;
        }

        @Bean Car car(Engine engine,
                @Value("${car.vendor}") String vendor,
                @Value("${car.model}") String model
        ) {
            var c = new Car();
            c.setVendor(vendor);
            c.setModel(model);
            c.setEngine(engine);
            return c;
        }

        @Bean Engine engine(@Value("${engine.fuel}") String fuel) {
            var e = new Engine();
            e.setFuel(fuel);
            return e;
        }
    }

}
