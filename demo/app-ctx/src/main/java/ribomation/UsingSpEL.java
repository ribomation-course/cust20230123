package ribomation;

import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.Map;

public class UsingSpEL {
    public static void main(String[] args) {
        var app = new UsingSpEL();
        app.ex1();
    }

    void ex1() {
        var p = new SpelExpressionParser();
        var r = p.parseExpression(" 'hello'.toUpperCase() + '-' + 'WORLD'.toLowerCase() ");
        System.out.println(r.getValue());
    }

}
