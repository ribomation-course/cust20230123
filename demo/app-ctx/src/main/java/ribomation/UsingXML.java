package ribomation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsingXML {
    public static void main(String[] args) {
        var ctx = new ClassPathXmlApplicationContext("/beans.xml");
        var obj = ctx.getBean("nisse");
        System.out.printf("obj: %s%n", obj);
    }
}
