package ribomation;

import org.springframework.context.support.GenericGroovyApplicationContext;

public class UsingGroovy {
    public static void main(String[] args) {
        var ctx = new GenericGroovyApplicationContext("/beans.groovy");
        var obj = ctx.getBean("anna");
        System.out.printf("obj: %s%n", obj);
    }
}
