package ribomation;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class UsingResource {
    public static void main(String[] args) {
        var ctx = new ClassPathXmlApplicationContext("app.xml");
        var loader = ctx.getBean("loader", Loader.class);
        loader.load();
        loader.accounts.forEach(System.out::println);
    }

    static class Loader {
        Resource input;
        List<Account> accounts;
        public Loader(Resource input) {this.input = input;}
        void load() {
            try (var is = input.getInputStream()) {
                var in = new BufferedReader(new InputStreamReader(is));
                accounts = in.lines()
                        .skip(1)
                        .map(line -> line.split(";"))
                        .map(f -> Account.create(f[0], f[1], f[2], f[3], f[4]))
                        .collect(Collectors.toList());
            } catch (IOException e) {throw new RuntimeException(e);}
        }
    }

    static class Account {
        //accno;balance;customer;email;city
        String accno;
        double balance;
        String customer;
        String email;
        String city;

        static Account create(String a, String b, String c, String e, String y) {
            var acc = new Account();
            acc.accno = a;
            acc.balance = Double.parseDouble(b);
            acc.customer = c;
            acc.email = e;
            acc.city = y;
            return acc;
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", Account.class.getSimpleName() + "[", "]")
                    .add("accno='" + accno + "'")
                    .add("balance=" + balance)
                    .add("customer='" + customer + "'")
                    .add("email='" + email + "'")
                    .add("city='" + city + "'")
                    .toString();
        }
    }

}
