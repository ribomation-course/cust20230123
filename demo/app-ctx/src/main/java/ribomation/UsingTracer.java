package ribomation;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ribomation.domain.Car;
import ribomation.domain.Engine;
import ribomation.domain.Person;

public class UsingTracer {
    public static void main(String[] args) {
        var ctx = new AnnotationConfigApplicationContext(UsingTracer.Beans.class);
        System.out.println(ctx.getBean("nisse"));
    }

    @Configuration
    static class Beans {
        @Bean BeanPostProcessor tracer() {
            return new BeanCreationTracer();
        }

        @Bean Person nisse(Car car) {
            var p = new Person();
            p.setName("Nisse Hult");
            p.setCar(car);
            return p;
        }

        @Bean Car jeep(Engine engine) {
            var c = new Car();
            c.setVendor("Jeep");
            c.setModel("Cherokee");
            c.setEngine(engine);
            return c;
        }

        @Bean Engine engine() {
            var e = new Engine();
            e.setFuel("diesel");
            return e;
        }
    }

}

