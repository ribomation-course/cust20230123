package ribomation.domain;

import java.util.StringJoiner;

public class Person {
    private String name;
    private Pet pet;
    private Car car;

    public Person() {}

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("pet=" + pet)
                .add("car=" + car)
                .toString();
    }

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public Car getCar() {return car;}
    public void setCar(Car car) {this.car = car;}
    public Pet getPet() {return pet;}
    public void setPet(Pet pet) {this.pet = pet;}
}

