package ribomation.domain;

import java.util.StringJoiner;

public class Car {
    private String vendor;
    private String model;
    private Engine engine;

    public Car() {}

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("vendor='" + vendor + "'")
                .add("model='" + model + "'")
                .add("engine=" + engine)
                .toString();
    }

    public String getVendor() {
        return vendor;
    }
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public Engine getEngine() {
        return engine;
    }
    public void setEngine(Engine engine) {
        this.engine = engine;
    }
}
