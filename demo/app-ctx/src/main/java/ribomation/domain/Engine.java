package ribomation.domain;

import java.util.StringJoiner;

public class Engine {
    private String fuel;

    public Engine() {
        this.fuel = "Diesel";
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Engine.class.getSimpleName() + "[", "]")
                .add("fuel='" + fuel + "'")
                .toString();
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }
}
