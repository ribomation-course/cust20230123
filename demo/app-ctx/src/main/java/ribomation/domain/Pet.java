package ribomation.domain;

import java.util.StringJoiner;

public class Pet {
    protected String name;

    public Pet(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
