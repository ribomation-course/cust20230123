package ribomation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ribomation.domain.*;


public class UsingJava {
    public static void main(String[] args) {
        var ctx = new AnnotationConfigApplicationContext(Beans.class);
        var obj = ctx.getBean("per");
        System.out.printf("obj: %s%n", obj);
    }

    @Configuration
    static class Beans {
        @Bean Person per(Pet pet, Car car) {
            var p = new Person();
            p.setName("Per Silja");
            p.setPet(pet);
            p.setCar(car);
            return p;
        }

        @Bean Pet pet() {
            var p = new Cat();
            p.setName("Yacc");
            return p;
        }

        @Bean Car car(Engine engine) {
            var c = new Car();
            c.setVendor("Tesla");
            c.setModel("Model S");
            c.setEngine(engine);
            return c;
        }

        @Bean Engine engine() {
            var e = new Engine();
            e.setFuel("electricity");
            return e;
        }
    }

}
