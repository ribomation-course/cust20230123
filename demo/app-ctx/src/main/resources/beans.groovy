import ribomation.domain.Car
import ribomation.domain.Cat
import ribomation.domain.Engine
import ribomation.domain.Person

beans {
    fuel(Engine) {
        fuel = 'petrol'
    }

    car(Car) {
        vendor = 'Toyota'
        model = 'Yaris'
        engine = fuel
    }

    cat(Cat) {
        name = 'Jaskel'
    }

    anna(Person) {
        name = 'Anna Conda'
        pet = cat
        car = car
    }
}

