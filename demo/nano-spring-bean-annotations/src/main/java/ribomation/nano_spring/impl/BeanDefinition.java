package ribomation.nano_spring.impl;

import java.util.List;

public final class BeanDefinition {
    public final String beanClass;
    public final List<String> constructorArgs;
    public BeanDefinition(String beanClass, List<String> constructorArgs) {
        this.beanClass       = beanClass;
        this.constructorArgs = constructorArgs;
    }
}
