package ribomation.nano_spring.impl;

import ribomation.nano_spring.Bean;
import ribomation.nano_spring.BeanRepository;
import ribomation.nano_spring.Inject;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class BeanRepositoryImpl implements BeanRepository {
    private final Map<String, Class> definitions = new HashMap<>();
    private final Map<String, Object> beans = new HashMap<>();

    public Map<String, Class> getDefinitions() {
        return definitions;
    }

    public Map<String, Object> getBeans() {
        return beans;
    }

    public void register(String className) {
        if (!definitions.containsKey(className)) {
            try {
                definitions.put(className, Class.forName(className));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public <T> T getBeanByType(String beanClassName, Class<T> beanClass) {
        var maybe = beans.values().stream()
                .filter(obj -> beanClassName.equals(obj.getClass().getName()))
                .findFirst();
        if (maybe.isPresent()) {
            return beanClass.cast(maybe.get());
        }
        return instantiateBean(beanClass);
    }

    @Override
    public <T> T getBean(String beanName, Class<T> beanClass) {
        if (beans.containsKey(beanName)) {
            return beanClass.cast(beans.get(beanName));
        }
        return instantiateBean(beanClass);
    }

    <T> T instantiateBean(Class<T> beanClass) {
        var wrap = instantiate(definitions.get(beanClass.getName()));
        beans.put(wrap.name, wrap.object);
        return beanClass.cast(wrap.object);
    }

    BeanWrapper instantiate(Class beanClass) {
        Bean annotation = (Bean) beanClass.getAnnotation(Bean.class);
        if (annotation == null) {
            throw new IllegalArgumentException("not annotated with @Bean: " + beanClass.getName());
        }

        try {
            var name = annotation.value().isBlank() ? beanClass.getSimpleName() : annotation.value();
            var constructor = beanClass.getConstructor();
            var bean = constructor.newInstance();
            injectFields(beanClass, bean);

            return new BeanWrapper(name, bean);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    void injectFields(Class beanClass, Object bean) {
        Stream.of(beanClass.getDeclaredFields())
                .filter(f -> f.getAnnotation(Inject.class) != null)
                .forEach(f -> {
                    try {
                        var name = f.getAnnotation(Inject.class).value();
                        var linkedType = f.getType();
                        Object linkedBean = name.isBlank() ? getBeanByType(linkedType.getName(), linkedType) : getBean(name, linkedType);
                        f.setAccessible(true);
                        f.set(bean, linkedBean);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }


}

