package ribomation.nano_spring;

import ribomation.nano_spring.impl.BeanRepositoryImpl;
import ribomation.nano_spring.impl.ClasspathLoader;

import java.util.Map;

public interface BeanRepository {
    <T> T getBean(String name, Class<T> T);
    <T> T getBeanByType(String type, Class<T> T);

    static BeanRepository createFromPackage(String packageName) {
        var repo   = new BeanRepositoryImpl();
        var loader = new ClasspathLoader(repo);
        loader.load(packageName);
        return repo;
    }

    Map<String, Object> getBeans();      //testing
    Map<String, Class> getDefinitions(); //testing
}

