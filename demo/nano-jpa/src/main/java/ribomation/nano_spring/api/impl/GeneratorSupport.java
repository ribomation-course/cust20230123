package ribomation.nano_spring.api.impl;

import ribomation.nano_spring.api.Column;
import ribomation.nano_spring.api.PrimaryKey;
import ribomation.nano_spring.api.Table;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GeneratorSupport {
    /**
     * Returns the table names based on the class name or value of @Table
     * @param domainClass   domain class
     * @return table name
     */
    public String getTableName(Class domainClass) {
        Table table = (Table) domainClass.getAnnotation(Table.class);
        return table.value().isBlank() ? domainClass.getSimpleName() + "s" : table.value();
    }

    /**
     * Returns all column names, sorted
     * @param domainClass   domain class
     * @param includePK     whether to include th primary key column or not
     * @return list of column names
     */
    public List<String> getColumnNames(Class domainClass, boolean includePK) {
        return Stream.of(domainClass.getDeclaredFields())
                .filter(f -> f.getAnnotation(Column.class) != null)
                .filter(f -> includePK || f.getAnnotation(PrimaryKey.class) == null)
                .map(Field::getName)
                .sorted()
                .collect(Collectors.toList());
    }

    public String toNames(List<String> columns) {
        return columns.stream().collect(Collectors.joining(","));
    }

    public String toQuestions(List<String> columns) {
        return columns.stream().map(n -> "?").collect(Collectors.joining(","));
    }

    /**
     * Returns the column name annotated with @PrimaryKey
     * @param domainClass domain class
     * @return name of column, or throws
     */
    public String getPrimaryKey(Class domainClass) {
        return Stream.of(domainClass.getDeclaredFields())
                .filter(f -> f.getAnnotation(Column.class) != null)
                .filter(f -> f.getAnnotation(PrimaryKey.class) != null)
                .map(Field::getName)
                .findFirst()
                .orElseThrow();
    }
}

