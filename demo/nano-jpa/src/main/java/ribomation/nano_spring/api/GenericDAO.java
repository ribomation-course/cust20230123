package ribomation.nano_spring.api;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public interface GenericDAO<DomainClass> extends AutoCloseable {
    void                  createTable();
    void                  close();
    int                   count();
    void                  withConnection(Consumer<Connection> conn);
    DomainClass           insert(DomainClass domainObject);
    void                  remove(DomainClass domainObject);
    Optional<DomainClass> findById(long id);
    List<DomainClass>     all();
}

