package ribomation.nano_spring.api.impl;

import java.util.List;

public class DQL_Generator extends GeneratorSupport {
    public static final String FIND     = "findBy";
    public static final String FIND_ALL = "findAllBy";

    public String generateSelectAll(String table) {
        return String.format("SELECT * FROM %s", table);
    }

    public String generateCountAll(String table) {
        return String.format("SELECT count(*) FROM %s", table);
    }

    public String generateSelect(String table, List<String> columns, String property) {
        return String.format("SELECT %s FROM %s WHERE %s = ?",
                toNames(columns), table, property);
    }
}


