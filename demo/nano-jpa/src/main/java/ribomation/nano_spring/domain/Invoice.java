package ribomation.nano_spring.domain;

import ribomation.nano_spring.api.*;

import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

@Table
public class Invoice {
    @Column @PrimaryKey
    private long id;

    @Column @Default("CURRENT_TIMESTAMP")
    private Date invoiced;

    @Column @NotNull
    private String customer;

    @Column @Default("0")
    private int amount;

    public Invoice() {}

    public Invoice(Date invoiced, String customer, int amount) {
        this(-1, invoiced, customer, amount);
    }

    public Invoice(long id, Date invoiced, String customer, int amount) {
        this.id = id;
        this.invoiced = invoiced;
        this.customer = customer;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Invoice.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("invoiced=" + String.format("%tF", invoiced))
                .add("customer='" + customer + "'")
                .add("amount=" + amount)
                .toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getInvoiced() {
        return invoiced;
    }

    public void setInvoiced(Date invoiced) {
        this.invoiced = invoiced;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return getId() == invoice.getId()
                && getAmount() == invoice.getAmount()
                && Objects.equals(getInvoiced(), invoice.getInvoiced())
                && Objects.equals(getCustomer(), invoice.getCustomer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getId(),
                getInvoiced(),
                getCustomer(),
                getAmount());
    }

}

