package ribomation;

import ribomation.domain.Cat;
import ribomation.domain.Engine;
import ribomation.domain.Person;
import ribomation.nano_spring.BeanRepository;

public class App {
    public static void main(String[] args) {
        try {
            App app = new App();
            app.init();
            app.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    BeanRepository beans;

    void init() {
        beans = BeanRepository.createFromResource("/beans.properties");
    }

    void run() {
        var obj1 = beans.getBean("anna", Person.class);
        System.out.printf("(1) obj1: %s%n", obj1);

        var obj2 = beans.getBean("nisse", Person.class);
        System.out.printf("(1) obj2: %s%n", obj2);

        var obj3 = beans.getBean("cat-3", Cat.class);
        obj2.setCat(obj3);
        System.out.printf("(2) obj1: %s%n", obj1);
        System.out.printf("(2) obj2: %s%n", obj2);

        var obj4 = beans.getBean("engine", Engine.class);
        obj4.setFuelType("Diesel");

        System.out.printf("(3) obj1: %s%n", obj1);
        System.out.printf("(3) obj2: %s%n", obj2);
    }
}
