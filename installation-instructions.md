# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the same setup as for the _Gradle_ course.

* SDKMAN
* Java JDK
* Gradle
* Intellij IDEA


## Node.js (_Optional_)
At the end of the course, there are some exercises involving Vue.js SPA clients.
If you want to perform these exercises, by building a Vue app using Vite,
you need to have Node.js installed as well.

* [Node.js & NPM/NPX](https://nodejs.org/en/)

N.B., it's possible to complete the exercises without Node.js, by just copy the
pre-built files. However, it would not be as fun :-)

