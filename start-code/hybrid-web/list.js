import { loadAll } from "/person-service.js";

function populateTR(item, tr) {
    tr.innerHTML = `
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.age}</td>
        <td>${item.female}</td>
        <td>${item.postCode}</td>
        <td> <a href="/show/${item.id}">Show</a> </td>
    `;
}

async function main() {
    const items = await loadAll();
    const tbody = document.querySelector('tbody');
    tbody.replaceChildren()
    for (const item of items) {
        const tr = document.createElement('tr');
        tbody.appendChild(tr);
        populateTR(item, tr);
    }
}

main().catch(console.error);
