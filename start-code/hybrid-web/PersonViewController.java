package xxx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ribomation.hybridweb.domain.PersonRepo;

@Controller
@RequestMapping(path = "/")
public class PersonViewController {
    @Autowired PersonRepo dao;

    @GetMapping(path = "show/{id}")
    public String show(Model m, @PathVariable int id) {
        //...
        return "show";
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String notFound(Exception x) {
        System.out.printf("ERR: %s%n", x);
        return "error";
    }

}
