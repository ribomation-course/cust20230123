package xxx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.hybridweb.domain.Person;
import ribomation.hybridweb.domain.PersonRepo;

import java.util.Map;

@RestController
@RequestMapping("/api/persons")
public class PersonApiController {
    @Autowired PersonRepo dao;

    @GetMapping
    public Iterable<Person> all() {
        return null; //...
    }

    @GetMapping("/{id}")
    public Person one(@PathVariable int id) {
        var empty = Person.of(-1, "", 0, false, 0);
        if (id == -1) return empty;
        return null; //...;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        //...
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public Map<String,String> notFound(Exception x) {
        System.out.printf("ERR: %s%n", x);
        return Map.of( "error", x.toString() );
    }

}
