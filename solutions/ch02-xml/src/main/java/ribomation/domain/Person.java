package ribomation.domain;

import java.util.Objects;

public class Person {
    //name;age;gender;postCode
    private int id;
    private String name;
    private int age;
    private boolean female;
    private int postCode;

    public Person(String name, int age, boolean female, int postCode) {
        this.name = name;
        this.age = age;
        this.female = female;
        this.postCode = postCode;
    }


    public static Person of(int id, String name, String age, String gender, String postCode) {
        return of(id, name, Integer.parseInt(age), gender.equals("Female"), Integer.parseInt(postCode));
    }

    public static Person of(int id, String name, int age, String gender, int postCode) {
        return of(id, name, age, gender.equals("Female"), postCode);
    }

    public static Person of(int id, String name, int age, boolean female, int postCode) {
        var p = new Person(name, age, female, postCode);
        p.setId(id);
        return p;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", female=" + female +
                ", postCode=" + postCode +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && female == person.female && postCode == person.postCode && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, female, postCode);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isFemale() {
        return female;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }
}
