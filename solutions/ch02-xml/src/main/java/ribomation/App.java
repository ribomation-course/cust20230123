package ribomation;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.domain.PersonRepo;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonCsvMapper;
import ribomation.domain.PersonJsonMapper;
import ribomation.jdbc.H2DataSourceBuilder;
import ribomation.jdbc.JdbcClassicPersonRepo;
import ribomation.jdbc.PersonJdbcMapper;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

public class App {
    public static void main(String[] args) throws IOException {
        var app = new App();
//        app.run();
        app.runXML();
    }

    private void runXML() throws IOException {
        var ctx = new ClassPathXmlApplicationContext("beans.xml");
        var csvResource = ctx.getResource("persons.csv");
        var loader = ctx.getBean(PersonLoader.class);
        var dao = ctx.getBean("repo", PersonRepo.class);
        var jsonMapper = ctx.getBean(PersonJsonMapper.class);
        var useCase = new UseCase(csvResource.getInputStream(), loader, dao, jsonMapper);
        useCase.run();
    }

//    void run() {
//        var csvResource = getCsvResource();
//        var loader      = getPersonLoader();
//        var dao         = getPersonRepo();
//        var jsonMapper  = getJsonMapper();
//        var useCase     = new UseCase(csvResource, loader, dao, jsonMapper);
//        useCase.run();
//    }
//
//    PersonRepo getPersonRepo() {
//        var repo = new JdbcClassicPersonRepo();
//        repo.setDataSource(getDataSource());
//        repo.setMapper(getJdbcMapper());
//        repo.createTable();
//        return repo;
//    }
//
//    PersonLoader getPersonLoader() {
//        var loader = new PersonLoader();
//        loader.setMapper(getCsvMapper());
//        return loader;
//    }
//
//    InputStream getCsvResource() {
//        return getClass().getResourceAsStream("/persons.csv");
//    }
//    Connection getDataSource() {
//        return new H2DataSourceBuilder().openServer();
//    }
//    PersonCsvMapper getCsvMapper() {
//        return new PersonCsvMapper(";");
//    }
//    PersonJdbcMapper getJdbcMapper() {
//        return new PersonJdbcMapper();
//    }
//    PersonJsonMapper getJsonMapper() {
//        return new PersonJsonMapper();
//    }


}

