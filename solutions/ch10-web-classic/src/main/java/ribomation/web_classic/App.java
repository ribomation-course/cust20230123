package ribomation.web_classic;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import ribomation.web_classic.domain.PersonLoader;
import ribomation.web_classic.domain.PersonRepo;

import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    InputStream csvResource(ApplicationContext ctx) {
        try {
            return ctx.getResource("classpath:/persons.csv").getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    CommandLineRunner populate(InputStream csvResource, PersonLoader loader, PersonRepo dao) {
        return args -> {
            var list = loader.load(csvResource);
            dao.saveAll(list);
        };
    }
}
