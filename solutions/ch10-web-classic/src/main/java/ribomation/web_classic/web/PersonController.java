package ribomation.web_classic.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ribomation.web_classic.domain.PersonRepo;

@Controller
@RequestMapping(path = "/")
public class PersonController {
    @Autowired PersonRepo dao;

    @GetMapping
    public String index() {
        return "redirect:/list";
    }

    @GetMapping(path = "list")
    public String list(Model m) {
        m.addAttribute("list", dao.findAll());
        return "list";
    }

    @GetMapping(path = "show/{id}")
    public String show(Model m, @PathVariable int id) {
        m.addAttribute("item", dao.findById(id).orElseThrow());
        return "show";
    }

    @GetMapping(path = "remove/{id}")
    public String remove(Model m, @PathVariable int id) {
        dao.deleteById(id);
        return "redirect:/list";
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String error(Exception x) {
        System.out.printf("ERR: %s%n", x);
        return "error";
    }
}
