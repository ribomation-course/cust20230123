DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS accounts;

CREATE TABLE accounts
(
    accno     VARCHAR(50) PRIMARY KEY,
    balance   DECIMAL(7, 2),
    customer  VARCHAR(50),
    street    VARCHAR(50),
    post_code VARCHAR(50),
    city      VARCHAR(50),
    country   VARCHAR(50)
);

CREATE TABLE transactions
(
    id     INT PRIMARY KEY,
    date   DATE,
    accno  VARCHAR(50),
    amount INT,
    reason VARCHAR(250),
    FOREIGN KEY (accno) REFERENCES accounts (accno)
);

