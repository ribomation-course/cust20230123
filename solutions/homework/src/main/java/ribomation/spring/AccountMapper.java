package ribomation.spring;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import ribomation.domain.Account;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class AccountMapper implements RowMapper<Account> {
    @Override
    public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
        var obj = new Account();

        obj.setAccno(rs.getString("accno"));
        obj.setBalance(rs.getDouble("balance"));
        obj.setCustomer(rs.getString("customer"));
        obj.setStreet(rs.getString("street"));
        obj.setPostCode(rs.getString("post_code"));
        obj.setCity(rs.getString("city"));

        return obj;
    }
}
