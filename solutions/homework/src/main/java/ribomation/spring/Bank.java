package ribomation.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Bank {
    private final String name;
    private final String city;

    public Bank(
            @Value("${bank.name}") String name,
            @Value("${bank.city}") String city
    ) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }
}
