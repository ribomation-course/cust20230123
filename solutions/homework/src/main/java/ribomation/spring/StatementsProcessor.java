package ribomation.spring;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ribomation.domain.Account;
import ribomation.domain.AccountStatement;
import ribomation.domain.Transaction;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Component
public class StatementsProcessor implements InitializingBean {
    private final Path dir;
    private final String namePattern;
    private final DAO dao;
    private final Bank bank;

    public StatementsProcessor(
            @Value("${statements.dir}") String dir,
            @Value("${statements.file.pattern}") String namePattern,
            DAO dao,
            Bank bank
    ) {
        this.dir = Path.of(dir);
        this.namePattern = namePattern;
        this.dao = dao;
        this.bank = bank;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (Files.exists(dir)) {
            try (var s = Files.walk(dir)) {
                s.filter(Files::isRegularFile).forEach(p -> {
                    try {
                        Files.delete(p);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
            Files.delete(dir);
        }
        Files.createDirectories(dir);
    }

    public void process() {
        var row = 0;
        for (Account a : dao.findAllAccounts()) {
            processAccount(++row, a, dao.findAllTransactionsByAccno(a.getAccno()));
        }
        System.out.printf("Processed %d accounts%n", row);
    }

    void processAccount(int row, Account account, List<Transaction> transactions) {
        var customer = account.getCustomer();
        var lname = customer.substring(customer.lastIndexOf(" ") + 1);
        var file = String.format(namePattern, row, lname).toLowerCase();

        var stmt = new AccountStatement(bank, account, transactions);
        try (var w = Files.newBufferedWriter(dir.resolve(file))) {
            stmt.printTo(new PrintWriter(w));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
