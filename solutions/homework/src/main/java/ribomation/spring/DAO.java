package ribomation.spring;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ribomation.domain.Account;
import ribomation.domain.Transaction;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class DAO {
    private final JdbcTemplate jdbc;
    private final RowMapper<Account> accountMapper;
    private final RowMapper<Transaction> transactionMapper;

    public DAO(DataSource ds, RowMapper<Account> accountMapper, RowMapper<Transaction> transactionMapper) {
        this.jdbc = new JdbcTemplate(ds);
        this.accountMapper = accountMapper;
        this.transactionMapper = transactionMapper;
    }

    public List<Account> findAllAccounts() {
        var sql = "SELECT * FROM accounts";
        return jdbc.query(sql, accountMapper);
    }

    public List<Transaction> findAllTransactionsByAccno(String accno) {
        var sql = "SELECT * FROM transactions WHERE accno = ?";
        return jdbc.query(sql, transactionMapper, accno);
    }

}
