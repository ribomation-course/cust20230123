package ribomation.spring;

import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties extends PropertySourcesPlaceholderConfigurer {
    public ApplicationProperties() {
        super.setLocation(new ClassPathResource("application.properties"));
    }
}
