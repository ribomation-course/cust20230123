package ribomation.spring;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import ribomation.domain.Transaction;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class TransactionMapper implements RowMapper<Transaction> {
    @Override
    public Transaction mapRow(ResultSet rs, int rowNum) throws SQLException {
        var obj = new Transaction();

        obj.setId(rs.getLong("id"));
        obj.setDate(rs.getDate("date"));
        obj.setAccno(rs.getString("accno"));
        obj.setAmount(rs.getInt("amount"));
        obj.setReason(rs.getString("reason"));

        return obj;
    }
}
