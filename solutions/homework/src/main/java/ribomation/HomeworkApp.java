package ribomation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ribomation.spring.StatementsProcessor;

public class HomeworkApp {
    public static void main(String[] args) {
        var app = new HomeworkApp();
        app.setup();
        app.run();
    }

    ApplicationContext appCtx;

    void setup() {
        var ctx = new AnnotationConfigApplicationContext();
        ctx.scan("ribomation.spring");
        ctx.refresh();
        appCtx = ctx;
    }

    void run() {
        var sp = appCtx.getBean(StatementsProcessor.class);
        var start = System.currentTimeMillis();
        sp.process();
        var elapsed = System.currentTimeMillis() - start;
        System.out.printf("Elapsed time: %1.3f us%n", elapsed * 1E-3);
    }
}
