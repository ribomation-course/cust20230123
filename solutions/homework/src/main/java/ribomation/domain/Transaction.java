package ribomation.domain;

//id     INT PRIMARY KEY,
//date   DATE,
//accno  VARCHAR(50),
//amount INT,
//reason VARCHAR(250),

import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

public class Transaction {
    private long id;
    private Date date;
    private String accno;
    private int amount;
    private String reason;

    public Transaction() {}

    @Override
    public String toString() {
        return new StringJoiner(", ", Transaction.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("date=" + date)
                .add("accno='" + accno + "'")
                .add("amount=" + amount)
                .add("reason='" + reason + "'")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id == that.id && amount == that.amount
               && date.equals(that.date)
               && accno.equals(that.accno)
               && reason.equals(that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, accno, amount, reason);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAccno() {
        return accno;
    }

    public void setAccno(String accno) {
        this.accno = accno;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
