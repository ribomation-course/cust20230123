package ribomation.domain;

//accno     VARCHAR(50) PRIMARY KEY,
//balance   DECIMAL(7, 2),
//customer  VARCHAR(50),
//street    VARCHAR(50),
//post_code VARCHAR(50),
//city      VARCHAR(50)

import java.util.Objects;
import java.util.StringJoiner;

public class Account {
    private String accno;
    private double balance;
    private String customer;
    private String street;
    private String postCode;
    private String city;

    public Account() {}

    @Override
    public String toString() {
        return new StringJoiner(", ", Account.class.getSimpleName() + "[", "]")
                .add("accno='" + accno + "'")
                .add("balance=" + balance)
                .add("customer='" + customer + "'")
                .add("street='" + street + "'")
                .add("post_code='" + postCode + "'")
                .add("city='" + city + "'")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Double.compare(account.getBalance(), getBalance()) == 0
               && getAccno().equals(account.getAccno())
               && getCustomer().equals(account.getCustomer())
               && getStreet().equals(account.getStreet())
               && getPostCode().equals(account.getPostCode())
               && getCity().equals(account.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccno(), getBalance(), getCustomer(), getStreet(), getPostCode(), getCity());
    }

    public String getAccno() {
        return accno;
    }

    public void setAccno(String accno) {
        this.accno = accno;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
