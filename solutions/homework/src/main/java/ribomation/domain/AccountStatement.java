package ribomation.domain;

import ribomation.spring.Bank;

import java.io.PrintWriter;
import java.util.List;

public class AccountStatement {
    private final Bank bank;
    private final Account account;
    private final List<Transaction> transactions;

    public AccountStatement(Bank bank, Account account, List<Transaction> transactions) {
        this.bank = bank;
        this.account = account;
        this.transactions = transactions;
    }

    public void printTo(PrintWriter out) {
        header(out);
        customer(out);
        double balance = openBalance(out);
        balance = transactions(out, balance);
        closeBalance(out, balance);
    }

    private void header(PrintWriter out) {
        out.println("--- Account Statement ---");
        out.printf("%s @ %s%n", bank.getName(), bank.getCity());
        out.println();
    }

    private double openBalance(PrintWriter out) {
        out.printf("ACCNO: %s%n", account.getAccno());
        var balance = account.getBalance();
        out.printf("Opening Balance: %63.2f%n", balance);
        return balance;
    }

    private void closeBalance(PrintWriter out, double balance) {
        out.printf("Closing Balance: %63.2f%n", balance);
    }

    private void customer(PrintWriter out) {
        out.println("Customer:");
        out.printf("%s%n", notNull(account.getCustomer()));
        out.printf("%s%n%s %s%n",
                notNull(account.getStreet()),
                notNull(account.getPostCode()), notNull(account.getCity()));
        out.println();
    }

    private double transactions(PrintWriter out, double balance) {
        out.println("--------------------------------------------------------------------------------");
        out.printf("%-12s %-30.30s %20s %15s%n", "Date", "Reason", "Amount", "Balance");
        out.println("--------------------------------------------------------------------------------");
        balance = do_transactions(out, balance);
        out.println("--------------------------------------------------------------------------------");
        return balance;
    }

    private double do_transactions(PrintWriter out, double balance) {
        if (transactions.isEmpty()) {
            out.println("    No transactions in this statement period");
        } else {
            for (Transaction t : transactions) {
                balance += t.getAmount();
                transaction(out, balance, t);
            }
        }
        return balance;
    }

    private void transaction(PrintWriter out, double balance, Transaction t) {
        out.printf("%-12tF %-36.36s %14d %15.2f%n", t.getDate(), t.getReason(), t.getAmount(), balance);
    }

    private String notNull(String s) {
        if (s == null) return "";
        return s.trim();
    }
}
