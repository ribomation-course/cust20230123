package ribomation;

import java.sql.DriverManager;

public class CheckDB {
    public static void main(String[] args) throws Exception {
        var app = new CheckDB();
        app.run();
    }

    void run() throws Exception {
        var url = "jdbc:postgresql://localhost:5432/accounts";
        var usr = "postgres";
        var pwd = "whatever";
        var con = DriverManager.getConnection(url, usr, pwd);
        try (con) {
            var stmt = con.createStatement();
            try (stmt) {
                var rs = stmt.executeQuery("SELECT 1 ");
                try (rs) {
                    if (rs.next()) {
                        System.out.printf("OK: %s%n", rs);
                    }
                }
            }
        }
    }

}
