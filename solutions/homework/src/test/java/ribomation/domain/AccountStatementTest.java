package ribomation.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ribomation.spring.Bank;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Random;

class AccountStatementTest {
    @Test
    @DisplayName("printing zero transactions")
    void test1() {
        var s = new AccountStatement(mkBank(), mkAccount(), List.of());

        var buf = new StringWriter(10_000);
        var out = new PrintWriter(buf);
        s.printTo(out);
        out.close();
        System.out.printf(Locale.forLanguageTag("sv-SE"), "%s", buf);
    }

    @Test
    @DisplayName("printing one transaction")
    void test2() {
        var d = LocalDate.now();
        var s = new AccountStatement(mkBank(), mkAccount(), List.of(mkTransaction(d, 0)));

        var buf = new StringWriter(10_000);
        var out = new PrintWriter(buf);
        s.printTo(out);
        out.close();
        System.out.println(buf);
    }

    @Test
    @DisplayName("printing three transactions")
    void test3() {
        var d = LocalDate.now();
        var s = new AccountStatement(mkBank(), mkAccount(), List.of(
                mkTransaction(d, 0),
                mkTransaction(d, 3),
                mkTransaction(d, 7)
        ));

        var buf = new StringWriter(10_000);
        var out = new PrintWriter(buf);
        s.printTo(out);
        out.close();
        System.out.println(buf);
    }

    private static Bank mkBank() {
        return new Bank("Banken", "Stockholm");
    }

    private static Account mkAccount() {
        var a = new Account();
        a.setAccno("1234-567890");
        a.setBalance(100);
        a.setCustomer("Anna Conda");
        a.setStreet("42 Hacker Lane");
        a.setPostCode("123 45");
        a.setCity("Perl Lake");
        return a;
    }

    private static Transaction mkTransaction(LocalDate base, int offset) {
        var d = base.plusDays(offset);
        var t = new Transaction();
        t.setDate(Date.valueOf(d));
        t.setAmount(10 + r.nextInt(200));
        t.setReason(reasons.get(nextReasonIndex++));
        return t;
    }

    private static final Random r = new Random();
    private static int nextReasonIndex = 0;
    private static final List<String> reasons = List.of(
            "Tremble wisely like an intelligent green people.",
            "Gravimetric space suits, to the universe.",
            "Chili tastes best with olive oil and lots of butter.",
            "You have to fail, and absorb peace by your occurring.",
            "The dosi is virtually modern."
    );

}
