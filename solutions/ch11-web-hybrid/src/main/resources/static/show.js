import { loadById, remove } from "/person-service.js";

function mkTR(key, val) {
    const tr = document.createElement('tr');
    tr.innerHTML = `
        <th>${key}</th> <td>${val}</td>
    `;
    return tr;
}

function populate(item, tbody) {
    tbody.appendChild(mkTR('ID', item.id));
    tbody.appendChild(mkTR('Name', item.name));
    tbody.appendChild(mkTR('Age', item.age));
    tbody.appendChild(mkTR('Female', item.female));
    tbody.appendChild(mkTR('Post Code', item.postCode));
}

async function main() {
    const tbody = document.querySelector('tbody');
    const id = tbody.dataset.id;
    const item = await loadById(id);
    populate(item, tbody);

    const removeBtn = document.querySelector('#remove')
    removeBtn.addEventListener('click', async (ev) => {
        ev.preventDefault();
        await remove(id);
        location.assign('/list')
    });
    removeBtn.style.display = 'inline-block';
}

main().catch(console.error);
