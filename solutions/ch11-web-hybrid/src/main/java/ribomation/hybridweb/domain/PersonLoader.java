package ribomation.hybridweb.domain;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonLoader {
    private final PersonCsvMapper mapper;

    public PersonLoader(PersonCsvMapper mapper) {
        this.mapper = mapper;
    }

    public List<Person> loadAll(InputStream is) {
        try (var in = new BufferedReader(new InputStreamReader(is))) {
            return in.lines()
                    .skip(1)
                    .map(mapper::fromCSV)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
