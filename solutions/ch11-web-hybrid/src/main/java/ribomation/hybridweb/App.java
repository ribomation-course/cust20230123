package ribomation.hybridweb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ribomation.hybridweb.domain.PersonLoader;
import ribomation.hybridweb.domain.PersonRepo;

import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
public class App implements WebMvcConfigurer {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/list");
        registry.addViewController("/list").setViewName("list");
    }

    @Bean
    InputStream csvResource(ApplicationContext ctx) {
        try {
            return ctx.getResource("classpath:/persons.csv").getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    CommandLineRunner populate(InputStream csvResource, PersonLoader loader, PersonRepo dao) {
        return args -> {
            var objs = loader.loadAll(csvResource);
            dao.saveAll(objs);
        };
    }

}
