package ribomation.webspa.domain;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Entity
public class Person {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 32)
    private String name;

    private Integer age;

    private Boolean female;

    private Integer postCode;

}
