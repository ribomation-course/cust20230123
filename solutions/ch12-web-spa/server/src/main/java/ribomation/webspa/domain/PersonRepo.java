package ribomation.webspa.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepo
        extends CrudRepository<Person, Integer> {

    List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper,
                                                              int postCodeUpper, boolean female);

}
