import { l as loadAll } from "./person-service-b73128cc.js";
import { r as ref, o as onMounted, a as resolveComponent, b as openBlock, c as createElementBlock, d as createBaseVNode, F as Fragment, e as renderList, t as toDisplayString, f as createVNode, w as withCtx, g as createTextVNode } from "./index-93c38e99.js";
const _hoisted_1 = /* @__PURE__ */ createBaseVNode("h1", null, "Person List", -1);
const _hoisted_2 = /* @__PURE__ */ createBaseVNode("thead", null, [
  /* @__PURE__ */ createBaseVNode("tr", null, [
    /* @__PURE__ */ createBaseVNode("th", null, "ID"),
    /* @__PURE__ */ createBaseVNode("th", null, "Name"),
    /* @__PURE__ */ createBaseVNode("th", null, "Age"),
    /* @__PURE__ */ createBaseVNode("th", null, "Female"),
    /* @__PURE__ */ createBaseVNode("th", null, "Post Code"),
    /* @__PURE__ */ createBaseVNode("th", null, "Actions")
  ])
], -1);
const _sfc_main = {
  __name: "list",
  setup(__props) {
    const items = ref([]);
    onMounted(async () => {
      items.value = await loadAll();
    });
    return (_ctx, _cache) => {
      const _component_router_link = resolveComponent("router-link");
      return openBlock(), createElementBlock(Fragment, null, [
        _hoisted_1,
        createBaseVNode("table", null, [
          _hoisted_2,
          createBaseVNode("tbody", null, [
            (openBlock(true), createElementBlock(Fragment, null, renderList(items.value, (item) => {
              return openBlock(), createElementBlock("tr", {
                key: item.id
              }, [
                createBaseVNode("td", null, toDisplayString(item.id), 1),
                createBaseVNode("td", null, toDisplayString(item.name), 1),
                createBaseVNode("td", null, toDisplayString(item.age), 1),
                createBaseVNode("td", null, toDisplayString(item.female), 1),
                createBaseVNode("td", null, toDisplayString(item.postCode), 1),
                createBaseVNode("td", null, [
                  createVNode(_component_router_link, {
                    to: "/show/" + item.id
                  }, {
                    default: withCtx(() => [
                      createTextVNode(" Show ")
                    ]),
                    _: 2
                  }, 1032, ["to"])
                ])
              ]);
            }), 128))
          ])
        ])
      ], 64);
    };
  }
};
export {
  _sfc_main as default
};
