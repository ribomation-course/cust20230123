const baseUrl = "http://localhost:8080/api/persons";
const json = "application/json";
const headers = { "Content-Type": json, "Accept": json };
async function loadAll() {
  const res = await fetch(baseUrl);
  return await res.json();
}
async function loadById(id) {
  const res = await fetch(`${baseUrl}/${id}`);
  return await res.json();
}
async function remove(id) {
  await fetch(`${baseUrl}/${id}`, {
    method: "DELETE",
    headers
  });
}
export {
  loadById as a,
  loadAll as l,
  remove as r
};
