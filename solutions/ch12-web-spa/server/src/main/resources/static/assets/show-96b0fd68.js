import { u as useRouter, r as ref, o as onMounted, b as openBlock, c as createElementBlock, d as createBaseVNode, t as toDisplayString, h as withModifiers, F as Fragment } from "./index-93c38e99.js";
import { a as loadById, r as remove } from "./person-service-b73128cc.js";
const _hoisted_1 = /* @__PURE__ */ createBaseVNode("h1", null, "Person Show", -1);
const _hoisted_2 = /* @__PURE__ */ createBaseVNode("th", null, "ID", -1);
const _hoisted_3 = /* @__PURE__ */ createBaseVNode("th", null, "Name", -1);
const _hoisted_4 = /* @__PURE__ */ createBaseVNode("th", null, "Age", -1);
const _hoisted_5 = /* @__PURE__ */ createBaseVNode("th", null, "Female", -1);
const _hoisted_6 = /* @__PURE__ */ createBaseVNode("th", null, "Post Code", -1);
const _sfc_main = {
  __name: "show",
  props: {
    id: String
  },
  setup(__props) {
    const props = __props;
    const router = useRouter();
    const item = ref({});
    onMounted(async () => {
      item.value = await loadById(props.id);
    });
    const removeItem = async (id) => {
      await remove(id);
      await router.replace({ name: "list" });
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock(Fragment, null, [
        _hoisted_1,
        createBaseVNode("table", null, [
          createBaseVNode("tbody", null, [
            createBaseVNode("tr", null, [
              _hoisted_2,
              createBaseVNode("td", null, toDisplayString(item.value.id), 1)
            ]),
            createBaseVNode("tr", null, [
              _hoisted_3,
              createBaseVNode("td", null, toDisplayString(item.value.name), 1)
            ]),
            createBaseVNode("tr", null, [
              _hoisted_4,
              createBaseVNode("td", null, toDisplayString(item.value.age), 1)
            ]),
            createBaseVNode("tr", null, [
              _hoisted_5,
              createBaseVNode("td", null, toDisplayString(item.value.female), 1)
            ]),
            createBaseVNode("tr", null, [
              _hoisted_6,
              createBaseVNode("td", null, toDisplayString(item.value.postCode), 1)
            ])
          ])
        ]),
        createBaseVNode("button", {
          onClick: _cache[0] || (_cache[0] = withModifiers(($event) => removeItem(item.value.id), ["prevent"]))
        }, " Remove this object ")
      ], 64);
    };
  }
};
export {
  _sfc_main as default
};
