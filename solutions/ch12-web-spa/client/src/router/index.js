import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {path: '/list', name: 'list', component: () => import('../pages/list.vue')},
    {path: '/show/:id', name: 'show', props: true, component: () => import('../pages/show.vue')},
    {path: '/', redirect: {name: 'list'}},
    {path: '/:pathMatch(.*)*', redirect: {name: 'list'}}
];


const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

export default router
