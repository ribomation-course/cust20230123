package ribomation.ch08config.domain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PersonJsonMapper {
    private final Gson gson;

    public PersonJsonMapper() {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public String toJson(Person person) {
        return gson.toJson(person);
    }

    public String toJson(List<Person> persons) {
        return gson.toJson(persons);
    }

}
