package ribomation.ch08config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import ribomation.ch08config.domain.PersonJsonMapper;
import ribomation.ch08config.domain.PersonLoader;
import ribomation.ch08config.domain.PersonRepo;

import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    InputStream csvResource(ApplicationContext ctx) {
        try {
            return ctx.getResource("classpath:/persons.csv").getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    UseCase useCase(InputStream csvResource, PersonLoader loader, PersonJsonMapper jsonMapper, PersonRepo dao) {
        return new UseCase(csvResource, loader, dao, jsonMapper);
    }

    @Bean
    CommandLineRunner doit(UseCase useCase/*, ApplicationConfig cfg*/) {
        return args -> {
//            System.out.println(cfg);
            useCase.run();
        };
    }
}
