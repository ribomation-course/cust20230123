package ribomation.ch08config.spring;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ribomation.ch08config.domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class SpringJdbcMapper implements RowMapper<Person> {
    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        var id = rs.getInt(1);
        var name = rs.getString(2);
        var age = rs.getInt(3);
        var gender = rs.getString(4);
        var postCode = rs.getInt(5);
        return Person.of(id, name, age, gender.equals("Female"), postCode);
    }
}
