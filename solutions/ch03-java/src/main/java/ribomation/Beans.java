package ribomation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ribomation.domain.PersonCsvMapper;
import ribomation.domain.PersonJsonMapper;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonRepo;
import ribomation.jdbc.H2DataSourceBuilder;
import ribomation.jdbc.JdbcClassicPersonRepo;
import ribomation.jdbc.PersonJdbcMapper;

import java.sql.Connection;

@Configuration
public class Beans {
    @Bean
    public PersonJsonMapper jsonMapper() {
        return new PersonJsonMapper();
    }

    @Bean
    public PersonCsvMapper csvMapper() {
        return new PersonCsvMapper();
    }

    @Bean
    public PersonJdbcMapper jdbcMapper() {
        return new PersonJdbcMapper();
    }

    @Bean
    public PersonLoader loader(PersonCsvMapper mapper) {
        var obj = new PersonLoader();
        obj.setMapper(mapper);
        return obj;
    }

    @Bean
    public Connection dataSource() {
        var h2 = new H2DataSourceBuilder();
        return h2.openServer();
    }

    @Bean
    public PersonRepo repo(Connection ds, PersonJdbcMapper mapper) {
        var obj = new JdbcClassicPersonRepo();
        obj.setDataSource(ds);
        obj.setMapper(mapper);
        obj.createTable();
        return obj;
    }

}
