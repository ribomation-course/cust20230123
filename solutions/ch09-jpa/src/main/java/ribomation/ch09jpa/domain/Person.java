package ribomation.ch09jpa.domain;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Entity
public class Person {
    //name;age;gender;postCode

    @Id @GeneratedValue
    private Integer id;

    @Column(length = 32, nullable = false)
    private String name;

    private Integer age;
    private Boolean female;
    private Integer postCode;

}
