package ribomation.ch09jpa.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface PersonRepo extends CrudRepository<Person, Integer> {

    List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper, int postCodeUpper, boolean female);

}
