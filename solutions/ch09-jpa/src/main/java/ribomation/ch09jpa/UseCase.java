package ribomation.ch09jpa;

import ribomation.ch09jpa.domain.PersonJsonMapper;
import ribomation.ch09jpa.domain.PersonRepo;

public class UseCase {
    PersonRepo dao;
    PersonJsonMapper jsonMapper;

    public UseCase(PersonRepo dao, PersonJsonMapper jsonMapper) {
        this.dao         = dao;
        this.jsonMapper  = jsonMapper;
    }

    public void run() {
        System.out.printf("inserted %d persons%n", dao.count());

        var lst = dao.findByAgeBetweenAndPostCodeLessThanAndFemale(30, 40, 12_500, true);
        System.out.printf("found %d persons%n", lst.size());

        dao.deleteAll(lst);
        System.out.printf("removed %d persons%n", lst.size());
        System.out.printf("reduced size to %d persons%n", dao.count());

        var person = dao.findById(1).orElseThrow();
        System.out.printf("found %s%n", person);
        person.setName("Per Silja");
        person.setAge(42);
        person.setFemale(false);
        person.setPostCode(25_000);
        dao.save(person);
        System.out.printf("modified %s%n", dao.findById(1).orElseThrow());

        System.out.println(jsonMapper.toJson(lst));
    }
}
