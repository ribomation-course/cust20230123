package ribomation.ch09jpa;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import ribomation.ch09jpa.domain.PersonJsonMapper;
import ribomation.ch09jpa.domain.PersonLoader;
import ribomation.ch09jpa.domain.PersonRepo;

import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    InputStream csvResource(ApplicationContext ctx) {
        try {
            return ctx.getResource("classpath:/persons.csv").getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    UseCase useCase(InputStream csvResource, PersonLoader loader, PersonRepo dao, PersonJsonMapper jsonMapper) {
        var list = loader.load(csvResource);
        dao.saveAll(list);
        return new UseCase(dao, jsonMapper);
    }

    @Bean
    CommandLineRunner doit(UseCase useCase) {
        return args -> {
            useCase.run();
        };
    }
}
