package ribomation.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@Service
public class PersonLoader {
    public interface Handler {
        void onPersonReceived(Person person);
    }

    private PersonCsvMapper mapper;

    @Autowired
    public void setMapper(PersonCsvMapper mapper) {
        this.mapper = mapper;
    }

    public void load(Path personsCsvFile, Handler handler) {
        try {
            load(Files.lines(personsCsvFile), handler);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load(InputStream is, Handler handler) {
        load(new BufferedReader(new InputStreamReader(is)).lines(), handler);
    }

    private void load(Stream<String> lines, Handler handler) {
        lines
                .skip(1)
                .map(csv -> mapper.fromCSV(csv))
                .forEach(handler::onPersonReceived);
    }

}
