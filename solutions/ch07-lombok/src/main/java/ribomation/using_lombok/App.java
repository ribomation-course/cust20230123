package ribomation.using_lombok;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import ribomation.using_lombok.domain.Person;
import ribomation.using_lombok.domain.PersonLoader;

import java.io.InputStream;
import java.util.List;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    CommandLineRunner doit(ApplicationContext ctx, PersonLoader loader) {
        return args -> {
            var is = ctx.getResource("persons.csv").getInputStream();
            var personList = loader.loadAll(is);
            personList.stream()
                    .limit(10)
                    .forEach(System.out::println);
        };
    }

}
