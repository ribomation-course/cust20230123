package ribomation.spring;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ribomation.domain.Person;
import ribomation.domain.PersonRepo;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public class SpringPersonRepo implements PersonRepo, InitializingBean {
    private static final String TBL = "persons";
    private final JdbcTemplate jdbc;
    private final RowMapper<Person> mapper;
    private static final String tableSQL = """
                                           DROP TABLE IF EXISTS persons;
                                           CREATE TABLE persons (
                                               id          int primary key auto_increment,
                                               name        varchar(64),
                                               age         int,
                                               gender      varchar(6),
                                               postCode    int
                                           );
                                           """;

    public SpringPersonRepo(DataSource ds, RowMapper<Person> mapper) {
        this.jdbc = new JdbcTemplate(ds);
        this.mapper = mapper;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createTable();
    }

    public void createTable() {
        jdbc.execute(tableSQL);
    }

    @Override
    public int countAll() {
        var sql = "select count(*) from " + TBL;
        return jdbc.queryForObject(sql, Integer.class);
    }

    @Override
    public void insert(Collection<Person> personCollection) {
        var sql = "INSERT INTO " + TBL + " (name,age,gender,postCode) VALUES (?,?,?,?)";
        ;
        int size = 250;
        ParameterizedPreparedStatementSetter<Person> pss = (PreparedStatement ps, Person person) -> {
            ps.setString(1, person.getName());
            ps.setInt(2, person.getAge());
            ps.setString(3, person.isFemale() ? "Female" : "Male");
            ps.setInt(4, person.getPostCode());
        };
        jdbc.batchUpdate(sql, personCollection, size, pss);
    }

    @Override
    public List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper, int postCodeUpper) {
        var sql = "SELECT id,name,age,gender,postCode FROM " + TBL + " WHERE age BETWEEN ? AND ? AND postCode <= ? AND gender = 'Female'";
        return jdbc.query(sql, mapper, ageLower, ageUpper, postCodeUpper);
    }

    @Override
    public void delete(int id) {
        var sql = "DELETE FROM " + TBL + " WHERE id = ?";
        jdbc.update(sql, id);
    }

    @Override
    public Optional<Person> findById(int id) {
        var sql = "SELECT id,name,age,gender,postCode FROM " + TBL + " WHERE id=?";
        try {
            var obj = jdbc.queryForObject(sql, mapper, id);
            return Optional.ofNullable(obj);
        } catch (DataAccessException ignore) {
        }
        return Optional.empty();
    }

    @Override
    public void update(int id, Person p) {
        var sql = "UPDATE " + TBL + " SET name=?, age=?, gender=?, postCode=? WHERE id=?";
        jdbc.update(sql, p.getName(), p.getAge(),
                p.isFemale() ? "Female" : "Male", p.getPostCode(), id);
    }

    @Override
    public List<Person> findAll() {
        return null;
    }

    @Override
    @Deprecated
    public int insert(Person person) {
        return 0;
    }
}
