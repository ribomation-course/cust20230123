package ribomation.spring;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
public class H2DataSource extends DriverManagerDataSource {
    public H2DataSource() {
        super("jdbc:h2:tcp://localhost:9092/persons", "sa", "");
    }
}
