package ribomation.hello_boot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Ch06BootApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch06BootApplication.class, args);
    }

    @Bean
    CommandLineRunner doit() {
        return args -> {
            System.out.println("Hello from my 1st spring boot app");
        };
    }

}
