# Homework

## Synopsis

Load accounts and transactions into a PostgreSQL database and print out account statements.

## Preparation

### PostgreSQL

Install a PostgreSQL database, unless you already have one.

#### Using Docker
_If you have_ Docker installed, you can make it easy by invoking the Docker Compose script below.

    version: '3.8'
    services:
      db:
        image: postgres:alpine
        ports:
          - '5432:5432'
        environment:
          POSTGRES_DB: accounts
          POSTGRES_PASSWORD: mypwd

Save it to a file named `docker-compose.yml`, ensure the Docker daemon is running and invoke the following command

    docker compose up

#### Direct Installation
Follow the installation instructions in the article below:

* [How To Install PostgreSQL on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-22-04-quickstart)

#### Test the Connection

Find the latest JDBC driver for PostgreSQL on 
[mvnrepository.com](https://mvnrepository.com/artifact/org.postgresql/postgresql/42.5.1).
Create a `build.gradle` file and paste the driver dependency. 

    repositories { mavenCentral() }
    dependencies {
        runtimeOnly 'org.postgresql:postgresql:42.5.1'
    }

Write a very small Java program that connects to the database, to ensure this part works.

    var url = "jdbc:postgresql://localhost:5432/accounts";
    var usr = "postgres";
    var pwd = "mypwd";
    var con = DriverManager.getConnection(url, usr, pwd);
    try (con) {
        var stmt = con.createStatement();
        try (stmt) {
            var rs = stmt.executeQuery("SELECT 1");
            try (rs) {
                if (rs.next()) {
                    System.out.printf("OK: %s%n", rs);
                }
            }
        }
    }

### Data

Browse to [Mockaroo](https://mockaroo.com/) and look around. Also, watch their tutorial about
[How to generate datasets related by a foreign key using Mockaroo](https://youtu.be/S_oYFGhZSkQ).

Create a schema with one field named `accno` and type `IBAN`. 
Save it as CSV with 20 rows plus include the header.
   ![](img/hw-accno.png)

Then create it as a dataset, name it `account_numbers`.

Create a new schema for 20 accounts, according to the screenshot below and save as SQL.
Let field `accno` use type _Dataset Column_ and your dataset in _sequential_ order. Also ensure to restrict field
   `country` to just _Sweden_.
   ![](img/accounts.png)

Create another schema for 175 transactions, according to the screenshot below and save as SQL.
   Let the date interval be of type _SQL datetime_, and span the whole year 2022.
   Let field `accno` use type _Dataset Column_ and your dataset in _random_ order.
   ![](img/transactions.png)

Adapt the CREATE TABLE statements for PostgreSQL, with primary key and foreign key.

    colName colType PRIMARY KEY
    FOREIGN KEY(colName) REFERENCES tblName(colName)

Insert a DROP TABLE statement first, so you can run the script multiple times, 
to start over.

    DROP TABLE IF EXISTS transactions;
    DROP TABLE IF EXISTS accounts;
    CREATE TABLE accounts (. . .);
    CREATE TABLE transactions (. . .);

Load the DB schema into the database, then the datasets.
If you are using a database tool, such as Intellij::Database or DataGrip or similar,
just use the tool, else write a small Java program. 

## The Spring App

Create a Java program using Spring JdbcTemplate, that performs an account aggregation
and prints out a statement as a text file.

For each account:
1. Open a new text file within a specific directory. You decide about the filename.
2. Print out account metadata, such as customer name and address
3. Print out the opening balance
4. For each transaction associated with the account
   * Print out the transaction data and updated account balance
5. Finally, print out the closing balance of the account
6. Close the file
7. Print out a summary of the number of accounts processed and the elapsed time

Update the gradle build to create an _application JAR_ and run it on the command-line.

_N.B._, it's not required to make a pretty output, however can be fun to do so anyway.
Here's an example to whet your appetite.

      --- Account Statement ---
      Ebberöds Bank @ Grönköping
      
      Customer:
      Yard Carlick
      8999 Kipling Terrace
      739 30 Skinnskatteberg
      
      ACCNO: GI44 NBRS P0TY JSZS VEPG KWG
      Opening Balance:                                                          914,87
      --------------------------------------------------------------------------------
      Date         Reason                                       Amount         Balance
      --------------------------------------------------------------------------------
      2022-01-13   For Ellen                                        56          970,87
      2022-06-16   Hostel: Part III                                 45         1015,87
      2022-07-10   Jönssonligan på Mallorca                        114         1129,87
      2022-08-22   Revenge of the Nerds II: Nerds in Pa             66         1195,87
      2022-10-20   Signs                                           169         1364,87
      2022-01-16   Chad Hanna                                       16         1380,87
      2022-05-22   Diebuster "Top wo Narae 2"                      190         1570,87
      2022-06-22   Professional Gun, A (Mercenary, The)            190         1760,87
      --------------------------------------------------------------------------------
      Closing Balance:                                                         1760,87

